package xyz.quipex.gifservice.cache;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import xyz.quipex.gifservice.cache.dto.GenerateGifRequestDto;
import xyz.quipex.gifservice.cache.dto.GifsByQueryDto;
import xyz.quipex.gifservice.cache.validation.GenerateGifRequest;
import xyz.quipex.gifservice.users.validation.Query;

import java.util.List;

@RestController
@RequestMapping("cache")
public class CacheController {
	private CacheService cacheService;
	
	public CacheController(CacheService cacheService) {
		this.cacheService = cacheService;
	}
	
	@PostMapping("generate")
	public GifsByQueryDto generate(@RequestBody @GenerateGifRequest GenerateGifRequestDto request) {
		return cacheService.generateAndReturnAllGifsByQuery(request.getQuery());
	}
	
	@GetMapping
	public List<GifsByQueryDto> getFromCache(@RequestParam(required = false) @Query String query) {
		return cacheService.getAllGifsByQuery(query);
	}
	
	@DeleteMapping
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteCache() {
		cacheService.delete();
	}
}
