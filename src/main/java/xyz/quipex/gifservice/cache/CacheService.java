package xyz.quipex.gifservice.cache;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import xyz.quipex.gifservice.cache.dto.GifsByQueryDto;
import xyz.quipex.gifservice.giphy.GiphyService;
import xyz.quipex.gifservice.giphy.dto.GiphyGifDto;
import xyz.quipex.gifservice.giphy.dto.GiphySearchResponseDto;

import java.util.Collections;
import java.util.List;
import java.util.Set;

@Service
@Slf4j
public class CacheService {
	private GiphyService giphyService;
	private DiskCacheRepository diskCacheRepository;
	private DownloadService downloadService;
	
	public CacheService(GiphyService giphyService, DiskCacheRepository diskCacheRepository, DownloadService downloadService) {
		this.giphyService = giphyService;
		this.diskCacheRepository = diskCacheRepository;
		this.downloadService = downloadService;
	}
	
	/**
	 * @param query to search on giphy
	 * @return all gifs
	 */
	/*
		Скачать картинку из giphy.com и положить
		в соответствующую папку в кэше на диске.
		Вернуть объект со всеми картинками в
		кэше на диске.
	 */
	public GifsByQueryDto generateAndReturnAllGifsByQuery(String query) {
		GiphySearchResponseDto response = requestFromGiphy(query);
		for (GiphyGifDto gif : response.getGifs()) {
			var fileToSave = DiskCacheRepository.createSubfoldersAndReturnFile(query, gif.getId());
			downloadService.downloadGifToFile(fileToSave, gif.getId());
		}
		return diskCacheRepository.getAllGifsByQuery(query);
	}
	
	public List<GifsByQueryDto> getAllGifsByQuery(String query) {
		if (query != null && !query.isBlank()) {
			return Collections.singletonList(diskCacheRepository.getAllGifsByQuery(query));
		} else {
			return diskCacheRepository.getAllGifsByCategories();
		}
	}
	
	public Set<String> getAllGifs() {
		return diskCacheRepository.getAllGifs();
	}
	
	public void delete() {
		diskCacheRepository.deleteCacheDirectory();
	}
	
	/**
	 * Downloads gif by query and saves it
	 * @return paths of gifs
	 */
	public String generateGif(String query) {
		GiphySearchResponseDto response = requestFromGiphy(query);
		if (response.getGifs().size() == 0) {
			return null;
		} else {
			var gif = response.getGifs().get(0);
			var fileToSave = DiskCacheRepository.createSubfoldersAndReturnFile(query, gif.getId());
			downloadService.downloadGifToFile(fileToSave, gif.getId());
			return fileToSave.getPath();
		}
	}
	
	private GiphySearchResponseDto requestFromGiphy(String query) {
		var response = giphyService.random(query);
		log.debug("Got response from giphy: {}", response);
		return response;
	}
	
	public String getGifByQuery(String query) {
		return diskCacheRepository.getGifByCategory(query);
	}
}
