package xyz.quipex.gifservice.cache;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Repository;
import xyz.quipex.gifservice.cache.dto.GifsByQueryDto;
import xyz.quipex.gifservice.configuration.ApplicationException;
import xyz.quipex.gifservice.utils.PathFormatter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
@Slf4j
public class DiskCacheRepository {
	
	private static final String CACHE_ROOT = "cache";
	private Random random;
	
	public DiskCacheRepository(Random random) {
		this.random = random;
	}
	
	public static File createSubfoldersAndReturnFile(String subFolder, String fileName) {
		File savedFile = new File(CACHE_ROOT + "/" + subFolder + "/" + fileName + ".gif");
		//noinspection ResultOfMethodCallIgnored
		savedFile.getParentFile().mkdirs();
		log.debug("Made sure subfolders exist for the path {}", savedFile.getPath());
		return savedFile;
	}
	
	public GifsByQueryDto getAllGifsByQuery(String query) {
		log.debug("Getting all gifs from cache by query: {}", query);
		Set<String> gifPaths = getGifPathsFromFolder(CACHE_ROOT + "/" + query, true);
		return new GifsByQueryDto(query, gifPaths);
	}
	
	public List<GifsByQueryDto> getAllGifsByCategories() {
		log.debug("Getting all gifs from cache by categories");
		var cacheFolder = new File(CACHE_ROOT);
		File[] categoryFolders = cacheFolder.listFiles(File::isDirectory);
		if (categoryFolders == null) {
			return Collections.emptyList();
		}
		List<GifsByQueryDto> gifsList = new ArrayList<>();
		for (File folder : categoryFolders) {
			gifsList.add(getAllGifsByQuery(folder.getName()));
		}
		return gifsList;
	}
	
	public void deleteCacheDirectory() {
		try {
			log.debug("Deleting cache directory");
			FileUtils.deleteDirectory(new File(CACHE_ROOT));
		} catch (IOException e) {
			throw new ApplicationException(e);
		}
	}
	
	/**
	 * @return gifs paths, formatted with {@link PathFormatter#formatPath(Path)}
	 */
	public Set<String> getAllGifs() {
		log.debug("Getting all gifs from cache");
		return getGifPathsFromFolder(CACHE_ROOT, true);
	}
	
	/**
	 * @return gif path, not formatted
	 */
	public String getGifByCategory(String query) {
		log.debug("Getting random gif by '{}' category", query);
		Set<String> gifs = getGifPathsFromFolder(CACHE_ROOT + "/" + query, false);
		if (gifs.size() == 0) {
			return null;
		}
		int randomIndex = random.nextInt(gifs.size());
		return gifs.stream().skip(randomIndex).findFirst().orElseThrow(() -> new ApplicationException("It's broken"));
	}
	
	/**
	 * @param formatted if true then formats path with {@link PathFormatter#formatPath(Path)}
	 * @return set of paths to gifs under a folder
	 */
	private Set<String> getGifPathsFromFolder(String path, boolean formatted) {
		try (Stream<Path> allFiles = Files.walk(Paths.get(path))) {
			return collectGifPaths(allFiles, formatted);
		} catch (NoSuchFileException e) {
			return Collections.emptySet();
		} catch (IOException e) {
			throw new ApplicationException(e);
		}
	}
	
	private Set<String> collectGifPaths(Stream<Path> gifsStream, boolean formatted) {
		return gifsStream.filter(Files::isRegularFile)
				.filter(path -> path.getFileName().toString().endsWith(".gif"))
				.map(path -> {
					if (formatted) {
						return PathFormatter.formatPath(path);
					} else {
						return path.toString();
					}
				})
				.collect(Collectors.toSet());
	}
}
