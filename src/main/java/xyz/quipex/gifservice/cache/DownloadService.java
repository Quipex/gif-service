package xyz.quipex.gifservice.cache;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import xyz.quipex.gifservice.configuration.ApplicationException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;

@Service
@Slf4j
public class DownloadService {
	
	public void downloadGifToFile(File file, String gifId) {
		try (FileOutputStream outputStream = new FileOutputStream(file)) {
			String link = "https://i.giphy.com/media/" + gifId + "/giphy.gif";
			log.debug("Downloading from {}", link);
			var downloadByteChannel = Channels.newChannel(new URL(link).openStream());
			long bytesTransferred = outputStream.getChannel().transferFrom(downloadByteChannel, 0, Integer.MAX_VALUE);
			log.debug("{} bytes downloaded to {}", bytesTransferred, file.getPath());
		} catch (IOException e) {
			throw new ApplicationException(e);
		}
	}
}
