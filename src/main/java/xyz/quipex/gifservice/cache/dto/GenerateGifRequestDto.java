package xyz.quipex.gifservice.cache.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GenerateGifRequestDto {
	private String query;
	private String force;
}
