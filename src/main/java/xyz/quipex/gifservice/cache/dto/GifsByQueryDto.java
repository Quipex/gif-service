package xyz.quipex.gifservice.cache.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Objects;
import java.util.Set;

@Data
@AllArgsConstructor
public class GifsByQueryDto {
	private String query;
	private Set<String> gifs;
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		GifsByQueryDto that = (GifsByQueryDto) o;
//		Comparing only queries to avoid duplicates in xyz.quipex.gifservice.users.cache.UsersMemCacheRepository#addGif()
//		when explicitly adding this object into set every time user adds a gif
		return Objects.equals(query, that.query);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(query);
	}
}
