package xyz.quipex.gifservice.cache.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Generate gif request constraint
 */
@Documented
@Constraint(validatedBy = GenerateGifRequestValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface GenerateGifRequest {
	String message() default "The query must be minimum 1 character long excluding whitespaces";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
