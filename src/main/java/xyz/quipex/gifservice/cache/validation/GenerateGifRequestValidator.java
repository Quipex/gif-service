package xyz.quipex.gifservice.cache.validation;

import xyz.quipex.gifservice.cache.dto.GenerateGifRequestDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class GenerateGifRequestValidator implements ConstraintValidator<GenerateGifRequest, GenerateGifRequestDto> {
	@Override
	public void initialize(GenerateGifRequest constraintAnnotation) {
	
	}
	
	@Override
	public boolean isValid(GenerateGifRequestDto value, ConstraintValidatorContext context) {
		return value.getQuery() != null && !value.getQuery().isBlank();
	}
}
