package xyz.quipex.gifservice.configuration;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.Random;

@Configuration
@Slf4j
public class AppConfig {
	
	@Bean
	public ObjectMapper objectMapper() {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
		return objectMapper;
	}
	
	@Bean
	@Profile("dev")
	public Random devRandom() {
		long seed = System.currentTimeMillis();
		Random r = new Random(seed);
		log.warn("Using dev random bean (predictable!), seed: {}", seed);
		return r;
	}
	
	@Bean
	@Profile("prod")
	public Random prodRandom() {
		return new Random();
	}
}
