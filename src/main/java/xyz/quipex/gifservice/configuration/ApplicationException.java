package xyz.quipex.gifservice.configuration;

public class ApplicationException extends RuntimeException {
	public ApplicationException(String message) {
		super(message);
	}
	
	public ApplicationException(Throwable cause) {
		super(cause);
	}
}

