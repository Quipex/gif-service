package xyz.quipex.gifservice.configuration;

import org.apache.http.client.utils.URIBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.net.http.HttpClient;


@Configuration
public class HttpConfig {
	
	@Bean
	public HttpClient basicClient() {
		return HttpClient.newHttpClient();
	}
	
	@Bean
	@Scope("prototype")
	public URIBuilder uriBuilder() {
		return new URIBuilder();
	}
}
