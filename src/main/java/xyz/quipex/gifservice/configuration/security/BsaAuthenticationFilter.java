package xyz.quipex.gifservice.configuration.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class BsaAuthenticationFilter extends OncePerRequestFilter {
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		var bsaKey = request.getHeader("X-BSA-GIPHY");
		log.debug("Got request: {} {}; Header X-BSA-GIPHY: {}", request.getMethod(), request.getRequestURI(), bsaKey);
		if (bsaKey != null) {
			filterChain.doFilter(request, response);
		} else {
			response.sendError(403);
		}
	}
}
