package xyz.quipex.gifservice.gifs;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.quipex.gifservice.cache.CacheService;

import java.util.Set;

@RestController
public class GifController {
	private CacheService cacheService;
	
	public GifController(CacheService cacheService) {
		this.cacheService = cacheService;
	}
	
	@GetMapping("gifs")
	public Set<String> getAll() {
		return cacheService.getAllGifs();
	}
}
