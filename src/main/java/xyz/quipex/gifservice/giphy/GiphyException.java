package xyz.quipex.gifservice.giphy;

public class GiphyException extends RuntimeException {
	public GiphyException(String message) {
		super(message);
	}
	
	public GiphyException(Throwable cause) {
		super(cause);
	}
}
