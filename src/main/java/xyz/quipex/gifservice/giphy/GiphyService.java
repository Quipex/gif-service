package xyz.quipex.gifservice.giphy;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import xyz.quipex.gifservice.configuration.ApplicationException;
import xyz.quipex.gifservice.configuration.HttpException;
import xyz.quipex.gifservice.giphy.dto.GiphyResponseDto;
import xyz.quipex.gifservice.giphy.dto.GiphySearchResponseDto;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;


@Service
@Slf4j
public class GiphyService {
	private HttpClient httpClient;
	private URIBuilder uriBuilder;
	private ObjectMapper objectMapper;
	@Value("${app.giphy.api_key}")
	private String GIPHY_KEY;
	
	public GiphyService(HttpClient httpClient, URIBuilder uriBuilder, ObjectMapper objectMapper) {
		this.httpClient = httpClient;
		this.uriBuilder = uriBuilder;
		this.objectMapper = objectMapper;
	}
	
	/**
	 * @param query to search giphy
	 * @param limit of images per search
	 * @return response dto
	 */
	public GiphySearchResponseDto search(String query, int limit) {
		log.debug("Searching file(s) ({}) from giphy by query: {}", limit, query);
		var response = sendGet("api.giphy.com/v1/gifs/search",
				new BasicNameValuePair("api_key", GIPHY_KEY),
				new BasicNameValuePair("q", query),
				new BasicNameValuePair("limit", String.valueOf(limit)));
		return parseGiphyResponse(response, GiphySearchResponseDto.class);
	}
	
	public GiphySearchResponseDto random(String query) {
		log.debug("Random file from giphy by query: {}", query);
		var response = sendGet("api.giphy.com/v1/gifs/random",
				new BasicNameValuePair("api_key", GIPHY_KEY),
				new BasicNameValuePair("tag", query));
		return parseGiphyResponse(response, GiphySearchResponseDto.class);
	}
	
	private <T extends GiphyResponseDto> T parseGiphyResponse(HttpResponse<String> response, Class<T> classToPopulate) {
		try {
			if (response.statusCode() != 200) {
				throw new GiphyException("Can't perform search on giphy, response: " + response.body());
			}
			return objectMapper.readValue(response.body(), classToPopulate);
		} catch (JsonProcessingException e) {
			throw new ApplicationException(e);
		}
	}
	
	private HttpResponse<String> sendGet(String url, NameValuePair... params) {
		try {
			return httpClient.send(HttpRequest.newBuilder(getUri(url, params)).build(), HttpResponse.BodyHandlers.ofString());
		} catch (IOException e) {
			throw new GiphyException(e);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new GiphyException(e);
		}
	}
	
	
	private URI getUri(String url, NameValuePair... params) {
		try {
			return uriBuilder.setScheme("https").setHost(url).setParameters(params).build();
		} catch (URISyntaxException e) {
			throw new HttpException("Can't build url", e);
		}
	}
}
