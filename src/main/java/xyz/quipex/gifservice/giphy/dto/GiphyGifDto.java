package xyz.quipex.gifservice.giphy.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GiphyGifDto {
	private String type;
	private String id;
	private Images images;
	
	@Data
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Images {
		private Original original;
		
		@Data
		@JsonIgnoreProperties(ignoreUnknown = true)
		public static class Original {
			private String url;
			private String hash;
		}
	}
}
