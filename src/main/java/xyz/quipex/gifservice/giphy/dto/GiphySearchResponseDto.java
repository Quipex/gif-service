package xyz.quipex.gifservice.giphy.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class GiphySearchResponseDto implements GiphyResponseDto {
	@JsonProperty("data")
	private List<GiphyGifDto> gifs;
	private Pagination pagination;
	private Meta meta;
	
	@Data
	public static class Pagination {
		@JsonProperty("total_count")
		private long totalCount;
		private long count;
		private long offset;
	}
	
	@Data
	public static class Meta {
		private int status;
		private String msg;
		@JsonProperty("response_id")
		private String responseId;
	}
}
