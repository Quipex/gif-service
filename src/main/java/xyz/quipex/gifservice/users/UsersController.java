package xyz.quipex.gifservice.users;

import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import xyz.quipex.gifservice.cache.dto.GenerateGifRequestDto;
import xyz.quipex.gifservice.cache.dto.GifsByQueryDto;
import xyz.quipex.gifservice.cache.validation.GenerateGifRequest;
import xyz.quipex.gifservice.users.dto.HistoryEntryDto;
import xyz.quipex.gifservice.users.history.UsersHistoryService;
import xyz.quipex.gifservice.users.validation.Query;
import xyz.quipex.gifservice.users.validation.UserId;
import xyz.quipex.gifservice.utils.PathFormatter;

import java.util.List;
import java.util.Set;

@RestController
@Validated
@RequestMapping("user/{userId}")
public class UsersController {
	private UsersService usersService;
	private UsersHistoryService usersHistory;
	
	public UsersController(UsersService usersService, UsersHistoryService usersHistory) {
		this.usersService = usersService;
		this.usersHistory = usersHistory;
	}
	
	@GetMapping("all")
	public Set<GifsByQueryDto> allGifsByCategories(@PathVariable @UserId String userId) {
		return usersService.getAllGifsByCategories(userId);
	}
	
	@GetMapping("history")
	public List<HistoryEntryDto> getHistory(@PathVariable @UserId String userId) {
		return usersHistory.getHistory(userId);
	}
	
	@DeleteMapping("history/clean")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void cleanHistory(@PathVariable @UserId String userId) {
		usersHistory.clean(userId);
	}
	
	@GetMapping("search")
	public String searchGif(@PathVariable @UserId String userId,
							@RequestParam @Query String query,
							@RequestParam(required = false) String force) {
		return PathFormatter.formatPath(usersService.searchGif(userId, query, force));
	}
	
	@PostMapping("generate")
	public String generateGif(@PathVariable @UserId String userId,
							  @RequestBody @GenerateGifRequest GenerateGifRequestDto generateRequest) {
		return PathFormatter.formatPath(usersService.generateGif(userId, generateRequest.getQuery(), generateRequest.getForce()));
	}
	
	@DeleteMapping("reset")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void resetUsersCache(@PathVariable @UserId String userId,
								@RequestParam(required = false) @Query String query) {
		usersService.resetCache(userId, query);
	}
	
	@DeleteMapping("clean")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void cleanUserData(@PathVariable @UserId String userId) {
		usersService.cleanData(userId);
	}
}
