package xyz.quipex.gifservice.users;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import xyz.quipex.gifservice.cache.CacheService;
import xyz.quipex.gifservice.cache.dto.GifsByQueryDto;
import xyz.quipex.gifservice.configuration.NotFoundException;
import xyz.quipex.gifservice.users.cache.UsersDiskCacheRepository;
import xyz.quipex.gifservice.users.cache.UsersMemCacheRepository;
import xyz.quipex.gifservice.users.dto.HistoryEntryDto;
import xyz.quipex.gifservice.users.history.UsersHistoryService;

import java.util.Set;

@Service
@Slf4j
public class UsersService {
	private UsersMemCacheRepository usersMemCacheRepository;
	private UsersHistoryService usersHistoryService;
	private UsersDiskCacheRepository usersDiskCacheRepository;
	private CacheService cacheService;
	
	public UsersService(UsersMemCacheRepository usersMemCacheRepository, UsersHistoryService usersHistoryService, UsersDiskCacheRepository usersDiskCacheRepository, CacheService cacheService) {
		this.usersMemCacheRepository = usersMemCacheRepository;
		this.usersHistoryService = usersHistoryService;
		this.usersDiskCacheRepository = usersDiskCacheRepository;
		this.cacheService = cacheService;
	}
	
	public Set<GifsByQueryDto> getAllGifsByCategories(String userId) {
		return usersDiskCacheRepository.getAllUserGifsByCategories(userId);
	}
	
	/*
	1. Пользователь отправляет запрос на поиск GIF.
		Пользователь должен указать id - может быть любая строка,
		которую можно использовать в качестве пути в OS (необходимо добавить
		базовую валидацию по id), а так же ключевое слово,
		по которому можно найти GIF.
	2. Приложение пытается получить путь к GIF из кэша в
		памяти (если есть несколько, то выбрать случайную) и вернуть пользователю.
	3. Если в памяти нет подходящей GIF, то попробовать найти в
		папке пользователя и выполнить шаг 2.
	4. Если в папке пользователя нет подходящей GIF, вернуть 404.
	
		Выполнить поиск картинки. Если указан
		параметр force, то игнорировать кэш в
		памяти и сразу читать данные с диска.
		Добавить файл в кэш в памяти, если его
		еще там нет.
	 */
	public String searchGif(String userId, String query, String force) {
		if (force == null) {
			String memGif = usersMemCacheRepository.getGif(userId, query);
			if (memGif != null) {
				return memGif;
			}
		} else {
			log.debug("Ignoring memory cache, reading from user's disk cache.");
		}
		String cachedGif = usersDiskCacheRepository.getGif(userId, query);
		if (cachedGif == null) {
			throw new NotFoundException();
		}
		usersMemCacheRepository.addGif(userId, query, cachedGif);
		return usersMemCacheRepository.getGif(userId, query);
	}
	
	/*
	1. Пользователь отправляет запрос на генерацию GIF.
		Пользователь должен указать id - может быть любая строка, которую
		можно использовать в качестве пути в OS (необходимо добавить базовую валидацию по id),
		а так же ключевое слово, по которому можно найти GIF.
	2. Приложение пытается получить путь к GIF из кэша на диске (папка cache)
		(если есть несколько подходящих GIF, то выбрать случайную) и добавить его
		в папку пользователя (по id). Затем обновить данные в кэше (в памяти) и
		вернуть пользователю путь к GIF.
	3. Если приложение не нашло GIF на диске (папка cache), то нужно сделать запрос
		на giphy.com, сохранить GIF (в качестве имени файла можно использовать Id из ответа от giphy.com или UUID)
		в кэш на диске (папка cache) и выполнить шаг 2.
	4. Добавить запись о генерации в файл истории пользователя history.csv
		10-10-2020,mad,/path/to/1.gif
		10-12-2020,ball,/path/to/2.gif
		
		Сгенерировать GIF. Если указан параметр
		force, то игнорировать кэш на диске (папка
		cache) и сразу искать файл в giphy.com.
		Добавить файл в кэш на диске (папка
		cache). (Доп. задание: избежать
		дублирования GIF файлов в кэше)
	 */
	public String generateGif(String userId, String query, String force) {
		if (force == null) {
			String cachedGif = cacheService.getGifByQuery(query);
			if (cachedGif != null) {
				return cacheGifForUser(userId, query, cachedGif);
			}
		} else {
			log.debug("Ignoring disk cache, downloading straight from giphy");
		}
		String generatedGif = cacheService.generateGif(query);
		if (generatedGif == null) {
			throw new NotFoundException();
		}
		return cacheGifForUser(userId, query, generatedGif);
	}
	
	private String cacheGifForUser(String userId, String query, String cachedGif) {
		String userGif = usersDiskCacheRepository.copyGifToUserStorage(cachedGif, userId);
		usersMemCacheRepository.addGif(userId, query, userGif);
		usersHistoryService.addNewEntry(userId, HistoryEntryDto.now(query, userGif));
		return userGif;
	}
	
	/*
	Очистить кэш пользователя в памяти по
	ключу query. Если ключ не указан, то
	очистить все данные по пользователю в
	кэше в памяти.
	 */
	public void resetCache(String userId, String query) {
		if (query != null && !query.isBlank()) {
			usersMemCacheRepository.clearBy(userId, query);
		} else {
			usersMemCacheRepository.clearBy(userId);
		}
	}
	
	public void cleanData(String userId) {
		usersMemCacheRepository.clearBy(userId);
		usersDiskCacheRepository.removeBy(userId);
	}
	
}
