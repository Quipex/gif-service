package xyz.quipex.gifservice.users.cache;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Repository;
import xyz.quipex.gifservice.cache.dto.GifsByQueryDto;
import xyz.quipex.gifservice.configuration.ApplicationException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

@Repository
@Slf4j
public class UsersDiskCacheRepository {
	private static final String USERS_ROOT = "users";
	
	@SuppressWarnings("ConstantConditions")
	public Set<GifsByQueryDto> getAllUserGifsByCategories(String userId) {
		log.debug("Loading gifs by categories from '{}' user's folder", userId);
		File userFolder = new File(USERS_ROOT + "/" + userId);
		File[] userFolders = userFolder.listFiles(File::isDirectory);
		if (userFolders == null) {
			return Collections.emptySet();
		}
		Set<GifsByQueryDto> gifsByQueries = new HashSet<>();
		for (File categoryFolder : userFolders) {
			Set<String> gifPaths = new HashSet<>();
			for (File file : categoryFolder.listFiles((dir, name) -> name.endsWith(".gif"))) {
				String gifPath = "/" + USERS_ROOT + "/" + userId + "/" + categoryFolder.getName() + "/" + file.getName();
				gifPaths.add(gifPath);
			}
			gifsByQueries.add(new GifsByQueryDto(categoryFolder.getName(), gifPaths));
		}
		return gifsByQueries;
	}
	
	/**
	 * @return path of new user's file
	 */
	public String copyGifToUserStorage(String cachedGifPath, String userId) {
		log.debug("Copying from common cache '{}' to user's folder '{}'", cachedGifPath, userId);
		File cachedGifFile = new File(cachedGifPath);
		String fileName = cachedGifFile.getName();
		String categoryName = cachedGifFile.getParentFile().getName();
		File usersFile = new File(USERS_ROOT + "/" + userId + "/" + categoryName + "/" + fileName);
		try {
			FileUtils.copyFile(cachedGifFile, usersFile);
			log.debug("Copied from {} to {}", cachedGifFile.getPath(), usersFile.getPath());
			return usersFile.getPath();
		} catch (IOException e) {
			throw new ApplicationException(e);
		}
	}
	
	/**
	 * @return path if found any gif, otherwise null
	 */
	public String getGif(String userId, String categoryName) {
		log.debug("Loading gif from '{}' user's folder by '{}' category", userId, categoryName);
		try (Stream<Path> gifFiles = Files.walk(Paths.get(USERS_ROOT + "/" + userId + "/" + categoryName))) {
			Optional<Path> foundGif = gifFiles.filter(Files::isRegularFile).findAny();
			return foundGif.map(Path::toString).orElse(null);
		} catch (NoSuchFileException e) {
			return null;
		} catch (IOException e) {
			throw new ApplicationException(e);
		}
	}
	
	public void removeBy(String userId) {
		try {
			log.debug("Deleting user '{}' folder", userId);
			FileUtils.deleteDirectory(new File(USERS_ROOT + "/" + userId));
		} catch (IOException e) {
			throw new ApplicationException(e);
		}
	}
}
