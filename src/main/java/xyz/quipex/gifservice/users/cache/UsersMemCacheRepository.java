package xyz.quipex.gifservice.users.cache;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import xyz.quipex.gifservice.cache.dto.GifsByQueryDto;
import xyz.quipex.gifservice.configuration.ApplicationException;

import java.util.*;
import java.util.stream.Collectors;

@Repository
@Slf4j
public class UsersMemCacheRepository {
	private final ObjectMapper objectMapper;
	private final Random random;
	private Map<String, Set<GifsByQueryDto>> userIdToUserGifs = new HashMap<>();
	
	public UsersMemCacheRepository(ObjectMapper objectMapper, Random random) {
		this.objectMapper = objectMapper;
		this.random = random;
	}
	
	public void addGif(String userId, String query, String gifPath) {
		log.debug("Adding new gif {} to user's ({}) memory storage", gifPath, userId);
		log.debug("Storage before: {}", json());
		Set<GifsByQueryDto> usersGifsByCategories = userIdToUserGifs.getOrDefault(userId, new HashSet<>());
		GifsByQueryDto category = usersGifsByCategories.stream().filter(gifsByQueries -> gifsByQueries.getQuery().equals(query))
				.findFirst().orElse(new GifsByQueryDto(query, new HashSet<>()));
		category.getGifs().add(gifPath);
		usersGifsByCategories.add(category);
		userIdToUserGifs.putIfAbsent(userId, usersGifsByCategories);
		log.debug("Storage after: {}", json());
	}
	
	private String json() {
		try {
			return objectMapper.writeValueAsString(userIdToUserGifs);
		} catch (JsonProcessingException e) {
			throw new ApplicationException(e);
		}
	}
	
	/**
	 * @return gif in memory of specific user (if there are many - picks random). null if no gifs were stored.
	 */
	public String getGif(String userId, String query) {
		log.debug("Try to find gif of {} by {}", userId, query);
		log.debug("Storage looks like: {}", json());
		Set<GifsByQueryDto> gifsByQueries = userIdToUserGifs.get(userId);
		if (gifsByQueries == null) {
			return null;
		}
		List<String> foundGifs = gifsByQueries.stream()
				.filter(gifsByQuery -> gifsByQuery.getQuery().equals(query))
				.flatMap(gifs -> gifs.getGifs().stream())
				.collect(Collectors.toList());
		if (foundGifs.isEmpty()) {
			return null;
		}
		return foundGifs.get(random.nextInt(foundGifs.size()));
	}
	
	public void clearBy(String userId, String query) {
		log.debug("Clear mem by userid '{}' and query '{}'", userId, query);
		log.debug("Storage before: {}", json());
		Set<GifsByQueryDto> gifsByQueries = userIdToUserGifs.get(userId);
		if (gifsByQueries != null) {
			gifsByQueries.removeIf(queryToGifs -> queryToGifs.getQuery().equals(query));
		}
		log.debug("Storage after: {}", json());
	}
	
	public void clearBy(String userId) {
		log.debug("Clear mem by userid '{}'", userId);
		log.debug("Storage before: {}", json());
		userIdToUserGifs.remove(userId);
		log.debug("Storage after: {}", json());
	}
}
