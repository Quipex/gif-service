package xyz.quipex.gifservice.users.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HistoryEntryDto {
	private String date;
	private String query;
	private String gif;
	
	public static HistoryEntryDto now(String query, String gif) {
		return new HistoryEntryDto(LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")), query, gif);
	}
}
