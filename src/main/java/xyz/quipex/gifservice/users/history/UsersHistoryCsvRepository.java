package xyz.quipex.gifservice.users.history;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;
import org.springframework.stereotype.Repository;
import xyz.quipex.gifservice.configuration.ApplicationException;
import xyz.quipex.gifservice.users.dto.HistoryEntryDto;
import xyz.quipex.gifservice.utils.PathFormatter;

import java.io.*;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class UsersHistoryCsvRepository {
	
	private static final String HISTORY_FILE = "history.csv";
	private static final String USERS_ROOT = "users";
	
	private static String getHistoryPath(String userId) {
		return USERS_ROOT + "/" + userId + "/" + HISTORY_FILE;
	}
	
	public List<HistoryEntryDto> getHistory(String userId) {
		try (FileReader fr = new FileReader(getHistoryPath(userId))) {
			CSVReader reader = new CSVReader(fr);
			return reader.readAll().stream()
					.map(entry -> new HistoryEntryDto(
							entry[0],
							entry[1],
							entry[2]))
					.collect(Collectors.toList());
		} catch (FileNotFoundException e) {
			return Collections.emptyList();
		} catch (IOException | CsvException e) {
			throw new ApplicationException(e);
		}
	}
	
	public void clean(String userId) {
		File file = new File(getHistoryPath(userId));
		//noinspection ResultOfMethodCallIgnored
		file.delete();
	}
	
	public void addNewEntry(String userId, HistoryEntryDto entry) {
		File fileToWrite = new File(getHistoryPath(userId));
		try (FileWriter fw = new FileWriter(fileToWrite, true)) {
			CSVWriter writer = new CSVWriter(fw);
			writer.writeNext(new String[]{
					entry.getDate(),
					entry.getQuery(),
					PathFormatter.formatPath(entry.getGif())
			});
		} catch (IOException e) {
			throw new ApplicationException(e);
		}
	}
}
