package xyz.quipex.gifservice.users.history;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import xyz.quipex.gifservice.users.dto.HistoryEntryDto;

import java.util.List;

@Service
@Slf4j
public class UsersHistoryService {
	
	private UsersHistoryCsvRepository repository;
	
	public UsersHistoryService(UsersHistoryCsvRepository repository) {
		this.repository = repository;
	}
	
	public List<HistoryEntryDto> getHistory(String userId) {
		log.debug("Reading from user '{}' history", userId);
		return repository.getHistory(userId);
	}
	
	public void clean(String userId) {
		repository.clean(userId);
		log.debug("Deleted user's history");
	}
	
	public void addNewEntry(String userId, HistoryEntryDto entry) {
		log.debug("Writing new entry to history user: {}, entry: {}", userId, entry);
		repository.addNewEntry(userId, entry);
		log.debug("Written!");
	}
}
