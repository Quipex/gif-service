package xyz.quipex.gifservice.users.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Query constraint
 */
@Documented
@Constraint(validatedBy = QueryValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface Query {
	String message() default "The query must be minimum 1 character long excluding whitespaces";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
