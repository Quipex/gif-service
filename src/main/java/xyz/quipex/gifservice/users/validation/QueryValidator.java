package xyz.quipex.gifservice.users.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class QueryValidator implements ConstraintValidator<Query, String> {
	public void initialize(Query constraint) {
	}
	
	public boolean isValid(String query, ConstraintValidatorContext context) {
		if (query == null) {
			return true;
		}
		return !query.isBlank();
	}
}
