package xyz.quipex.gifservice.users.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;


/**
 * User id constraint. Checks max length and allowed symbols.
 */
@Documented
@Constraint(validatedBy = UserIdValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface UserId {
    String message() default "Invalid user id format. Max 30 characters. Only latin letters and digits";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
