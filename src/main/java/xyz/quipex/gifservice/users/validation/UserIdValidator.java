package xyz.quipex.gifservice.users.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UserIdValidator implements ConstraintValidator<UserId, String> {
	
	@Override
	public void initialize(UserId constraintAnnotation) {
	
	}
	
	@Override
	public boolean isValid(String userId, ConstraintValidatorContext context) {
		return userId.matches("[A-Za-z0-9]+")
				&& userId.length() <= 30;
	}
}
