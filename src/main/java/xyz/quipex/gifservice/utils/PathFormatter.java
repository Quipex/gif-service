package xyz.quipex.gifservice.utils;

import java.nio.file.Path;

public class PathFormatter {
	
	/**
	 * Makes sure the path starts with '/' and replaces '\' with '/'
	 */
	public static String formatPath(Path path) {
		return "/" + path.toString().replace('\\', '/');
	}
	
	/**
	 * Makes sure the path starts with '/' and replaces '\' with '/'
	 */
	public static String formatPath(String path) {
		if (path == null) {
			return null;
		}
		if (!path.startsWith("/")) {
			path = "/" + path;
		}
		return path.replace('\\', '/');
	}
}
