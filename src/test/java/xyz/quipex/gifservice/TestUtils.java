package xyz.quipex.gifservice;

import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import static io.restassured.RestAssured.given;

public class TestUtils {
	private static final String BASE_URI = "http://localhost:7070/";
	
	public static RequestSpecification correctHeader() {
		return noHeader().header("X-BSA-GIPHY", "REST_ASSURED");
	}
	
	public static RequestSpecification noHeader() {
		return given().baseUri(BASE_URI).contentType("application/json").log().all();
	}
	
	public static ResponseSpecification expect200() {
		return new ResponseSpecBuilder().expectStatusCode(200).build();
	}
}
