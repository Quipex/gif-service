package xyz.quipex.gifservice.cache;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import xyz.quipex.gifservice.cache.dto.GenerateGifRequestDto;

import static org.apache.http.HttpStatus.SC_FORBIDDEN;
import static org.apache.http.HttpStatus.SC_NO_CONTENT;
import static xyz.quipex.gifservice.TestUtils.*;
import static xyz.quipex.gifservice.cache.Endpoints.cache;
import static xyz.quipex.gifservice.cache.Endpoints.generateCache;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class CacheControllerTest {
	
	@Test
	void whenDelete_then402() {
		correctHeader().when().delete(cache)
				.then().statusCode(SC_NO_CONTENT);
	}
	
	@Test
	void whenDownloadAndCache_then200() {
		correctHeader().when().body(new GenerateGifRequestDto("cat", null)).post(generateCache)
				.then().spec(expect200());
	}
	
	@Test
	void whenGetAllCacheFromDisk_then200() {
		correctHeader().when().get(cache)
				.then().spec(expect200());
	}
	@Test
	void whenGetCacheByQueryFromDisk_then200() {
		correctHeader().queryParam("query", "cat").get(cache)
				.then().spec(expect200());
		
		correctHeader().queryParam("query", "invalidquery_qweqweqwe").get(cache)
				.then().spec(expect200());
	}
	
	@Test
	void whenNoHeader_then403() {
		noHeader().get(cache).then().statusCode(SC_FORBIDDEN);
	}
}
