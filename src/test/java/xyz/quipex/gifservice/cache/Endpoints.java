package xyz.quipex.gifservice.cache;

public final class Endpoints {
	public static final String cache = "/cache";
	public static final String generateCache = "/cache/generate";
}
