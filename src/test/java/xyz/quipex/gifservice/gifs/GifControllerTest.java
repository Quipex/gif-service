package xyz.quipex.gifservice.gifs;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static xyz.quipex.gifservice.TestUtils.correctHeader;
import static xyz.quipex.gifservice.gifs.Endpoints.gifs;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class GifControllerTest {

	@Test
	void whenGetAllGifs_then200() {
		correctHeader().when().get(gifs).then().statusCode(200);
	}
}
