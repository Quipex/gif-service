package xyz.quipex.gifservice.users;

public class Endpoints {
	public static final String allGifs = "/user/{userId}/all";
	public static final String history = "/user/{userId}/history";
	public static final String historyClean = "/user/{userId}/history/clean";
	public static final String search = "/user/{userId}/search";
	public static final String generate = "/user/{userId}/generate";
	public static final String reset = "/user/{userId}/reset";
	public static final String clean = "/user/{userId}/clean";
}
