package xyz.quipex.gifservice.users;

import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.Test;
import xyz.quipex.gifservice.TestUtils;
import xyz.quipex.gifservice.cache.dto.GenerateGifRequestDto;

import static org.apache.http.HttpStatus.*;
import static xyz.quipex.gifservice.users.Endpoints.*;

class UsersControllerTest {
	
	@Test
	void allGifsByCategories() {
		user("test").when().get(allGifs).then().statusCode(SC_OK);
	}
	
	@Test
	void getHistory() {
		user("test").when().get(history).then().statusCode(SC_OK);
	}
	
	@Test
	void cleanHistory() {
		user("test").when().delete(historyClean).then().statusCode(SC_NO_CONTENT);
	}
	
	@Test
	void searchGif() {
		user("test").when().get(search).then().statusCode(SC_BAD_REQUEST);
		user("test").queryParams("query", "cat")
				.when().get(search).then().statusCode(SC_OK);
		user("test").queryParams("query", "")
				.when().get(search).then().statusCode(SC_BAD_REQUEST);
		user("test").queryParams("query", "cat", "force", "kek")
				.when().get(search).then().statusCode(SC_OK);
		user("test").queryParams("query", "cat", "force", "")
				.when().get(search).then().statusCode(SC_OK);
	}
	
	@Test
	void generateGif() {
		user("test").body(new GenerateGifRequestDto("cat", "kek")).when().post(generate)
				.then().statusCode(SC_OK);
	}
	
	@Test
	void resetUsersCache() {
		user("test").queryParams("query", "cat").when().delete(reset)
				.then().statusCode(SC_NO_CONTENT);
		user("test").when().delete(reset)
				.then().statusCode(SC_NO_CONTENT);
	}
	
	@Test
	void cleanUserData() {
		user("test").when().delete(clean).then().statusCode(SC_NO_CONTENT);
	}
	
	@Test
	void invalidUserTest() {
		user(" ").when().get(allGifs).then().statusCode(SC_BAD_REQUEST);
	}
	
	private static RequestSpecification user(String userId) {
		return TestUtils.correctHeader().pathParam("userId", userId);
	}
}
